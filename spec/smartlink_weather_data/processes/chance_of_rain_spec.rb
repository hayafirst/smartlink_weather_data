require 'spec_helper'

RSpec.describe SmartlinkWeatherData::Processes::ChanceOfRain do
  let(:postal_code1) { '11111' }
  let(:postal_code2) { '22222' }
  let(:time) { "2015-02-03 11:13 AM".to_time }
  let(:day1) { Date.new(2015, 2, 3) }
  let(:day2) { Date.new(2015, 2, 4) }
  let(:day3) { Date.new(2015, 2, 5) }
  let(:mocked_result) { double :mocked_result, pop: 23, postal_code: postal_code1 }
  let(:postal_code1_result1) { model_class.create! date: day1, pop: 23, postal_code: postal_code1 }
  let(:postal_code1_result2) { model_class.create! date: day2, pop: 34, postal_code: postal_code1 }
  let(:postal_code1_result3) { model_class.create! date: day3, pop: 28, postal_code: postal_code1 }

  let(:postal_code2_result1) { model_class.create! date: day1, pop: 90, postal_code: postal_code2 }
  let(:postal_code2_result2) { model_class.create! date: day2, pop: 89, postal_code: postal_code2 }
  let(:postal_code2_result3) { model_class.create! date: day3, pop: 76, postal_code: postal_code2 }
  let(:db_results) { [] }

  model_class_access

  let(:result) { described_class.value postal_code1, postal_code2 }

  before(:each) do
    Timecop.freeze time
    allow(SmartlinkWeatherData::Processes::SaveData).to receive(:save).with(postal_code1).and_return [mocked_result]
    allow(SmartlinkWeatherData::Processes::SaveData).to receive(:save).with(postal_code2).and_return [mocked_result]
  end

  after(:each) do
    Timecop.return
  end

  class << self
    def it_returns_max_pop_of_three_on(postal_code)
      it "returns max pop for #{postal_code}" do
        expect(result).to include(send(postal_code) => max)
      end
    end

    def it_fetches_from_api_for(postal_code)
      it "fetches from API" do
        expect(SmartlinkWeatherData::Processes::SaveData).to receive(:save).with(
          send(postal_code)
        )
        result
      end

      it "fetches the result again" do
        new_records
        expect(result).to eq(send(postal_code) => max)
      end
    end
  end

  context "with postal_code1 has all 3 results" do
    let!(:db_content) { [postal_code1_result1, postal_code1_result2, postal_code1_result3] }
    let(:max) { 34 }

    it_returns_max_pop_of_three_on :postal_code1
  end

  context "with postal_code2 has all 3 results" do
    let!(:db_content) { [postal_code2_result1, postal_code2_result2, postal_code2_result3] }
    let(:max) { 90 }

    it_returns_max_pop_of_three_on :postal_code2
  end

  context "with postal_code1 less than 3 results" do
    let!(:db_content) { [ postal_code1_result1, postal_code1_result2 ] }
    let(:new_records) { [postal_code1_result1, postal_code1_result2, postal_code1_result3] }
    let(:max) { 34 }
    it_fetches_from_api_for :postal_code1
  end

  context "with postal_code2 less than 3 results" do
    let!(:db_content) { [ postal_code2_result1, postal_code2_result2 ] }
    let(:new_records) { [postal_code2_result1, postal_code2_result2, postal_code2_result3] }
    let(:max) { 90 }
    it_fetches_from_api_for :postal_code2
  end

  context "with postal_code1 returns no results" do
    let(:new_records) { [postal_code1_result1, postal_code1_result2, postal_code1_result3] }
    let(:max) { 34 }
    it_fetches_from_api_for :postal_code1
  end

  context "with postal_code2 returns no results" do
    let(:new_records) { [postal_code2_result1, postal_code2_result2, postal_code2_result3] }
    let(:max) { 90 }
    it_fetches_from_api_for :postal_code2
  end
end
