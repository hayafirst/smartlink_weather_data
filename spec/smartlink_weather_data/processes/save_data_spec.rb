require 'spec_helper'

RSpec.describe SmartlinkWeatherData::Processes::SaveData do
  describe ".save" do
    model_class_access
    let(:postal_code1) { '11111' }
    let(:postal_code2) { '22222' }
    let(:result1) { JSON.parse(File.read('spec/fixtures/sample_output1.json')) }

    let(:result2) { JSON.parse(File.read('spec/fixtures/sample_output2.json')) }
    let(:fetcher1) { double :fetcher1, fetch: result1 }
    let(:fetcher2) { double :fetcher1, fetch: result2 }
    let(:last) { result2['forecast']['simpleforecast']['forecastday'].last }
    let(:last_date) {  Date.new(last['date']['year'], last['date']['month'], last['date']['day']) }

    before(:each) do
      allow(SmartlinkWeatherData::Wunderground::Fetcher).to receive(:new).with(postal_code1).and_return fetcher1
      allow(SmartlinkWeatherData::Wunderground::Fetcher).to receive(:new).with(postal_code2).and_return fetcher2
    end

    context "new records" do
      before(:each) do
        described_class.save postal_code1, postal_code2
      end

      it "creates 8 new weather forecasts" do
        expect(model_class.count).to eq(8)
      end

      context "last record" do
        class << self
          def it_sets_temperrature_correctly(params)
            field = "#{params[:direction]}_#{params[:unit]}"
            it "sets #{field} correctly" do
              expect(last_record.send(field)).to eq(last[params[:direction].to_s][params[:unit].to_s].to_f)
            end
          end

          def it_sets_rain_volumn_correctly(params)
            field = "#{params[:type]}_#{params[:unit]}"
            unit = params[:unit] == :inch ? 'in' : 'mm'
            it "sets #{field} correctly" do
              expect(last_record.send(field)).to eq(last[params[:type].to_s][unit].to_f)
            end
          end
        end

        let(:last_record) do
          model_class.where(epoch: last['date']['epoch'].to_i, postal_code: postal_code2).last
        end

        it "sets time_zone correctly" do
          expect(last_record.time_zone).to eq(last['date']['tz_short'])
        end

        it "sets date correctly" do
          expect(last_record.date).to eq(last_date)
        end

        it "sets pop correctly" do
          expect(last_record.pop).to eq(last['pop'].to_f)
        end

        it_sets_rain_volumn_correctly type: :qpf_allday, unit: :inch
        it_sets_rain_volumn_correctly type: :qpf_allday, unit: :mm
        it_sets_rain_volumn_correctly type: :qpf_day, unit: :inch
        it_sets_rain_volumn_correctly type: :qpf_day, unit: :mm
        it_sets_rain_volumn_correctly type: :qpf_night, unit: :inch
        it_sets_rain_volumn_correctly type: :qpf_night, unit: :mm

        it_sets_temperrature_correctly direction: :low, unit: :fahrenheit
        it_sets_temperrature_correctly direction: :low, unit: :celsius
        it_sets_temperrature_correctly direction: :high, unit: :fahrenheit
        it_sets_temperrature_correctly direction: :high, unit: :celsius
      end
    end

    context "with existing record" do
      let!(:existing) do
        model_class.create! date: last_date, postal_code: postal_code2, pop: 10
      end

      it "creates 7 more record" do
        described_class.save postal_code1, postal_code2
        expect(model_class.count).to eq(8)
      end

      it "doesn't touch the existing record" do
        expect(existing.reload.epoch).to be_nil
      end

    end
  end
end
