require 'spec_helper'

RSpec.describe SmartlinkWeatherData::Models::Forecast do
  let(:model) { described_class.new }
  let(:date1) { Date.new(2015, 6, 7) }
  let(:postal_code1) { '22113' }
  let(:date2) { date1 }
  let(:postal_code2) { postal_code1 }

  describe "validations checking: " do
    class << self
      def it_requires(*attrs)
        valid_value = '111'
        valid_value = attrs.pop[:value] if(attrs.last.is_a?(Hash))
        attrs.each do |attr|
          context "presence check on #{attr}" do
            context "with a value" do
              it "is invalid" do
                model.send("#{attr}=", valid_value)
                model.valid?
                expect(model.errors[attr]).not_to include("can't be blank")
              end
            end

            context "without a value" do
              it "doesn't raise a presence error" do
                model.send("#{attr}=", "")
                expect(model).not_to be_valid
                expect(model.errors[attr]).to eq(["can't be blank"])
              end
            end
          end
        end
      end

      def it_is_valid
        it "is valid" do
          expect(forecast).to be_valid
        end
      end
    end
    it_requires :postal_code, :pop
    it_requires :date, value: Date.new(2015, 1, 12)

    describe "uniqness of date and postal_code" do
      let!(:existing_forecast1) { described_class.create! date: date1, postal_code: postal_code1}
      let(:forecast) { described_class.new  date: date2, postal_code: postal_code2}

      context "different postal_code, different date" do
        let(:postal_code2) { '11111' }
        let(:date2) { Date.new(2016, 5, 4) }
        it_is_valid
      end

      context "same postal_code different date" do
        let(:date2) { Date.new(2016, 5, 4) }
        it_is_valid
      end

      context "different postal_code, same date" do
        let(:postal_code2) { '11111' }
        it_is_valid
      end

      context "same postal_code and same date" do
        it "is invalid" do
          expect(forecast).not_to be_valid
          expect(forecast.errors[:date]).to eq([ "has already been taken" ])
        end
      end
    end
  end

  describe ".date_and_postal_code" do
    let(:postal_code2) { '33333' }
    let!(:forecast1) { described_class.create! date: date1, postal_code: postal_code1}
    let!(:forecast2) { described_class.create! date: date2, postal_code: postal_code2}

    it "finds the correct forecast object" do
      expect(described_class.date_and_postal_code(date1, postal_code1)).to eq([forecast1])
    end

    context "with array params" do
      let(:date2) { Date.new(2016, 1, 23) }

      it "returns all matching results" do
        expect(described_class.date_and_postal_code([date1, date2], [postal_code1, postal_code2]).to_set).to eq(Set[forecast1, forecast2])
      end
    end
  end
end
