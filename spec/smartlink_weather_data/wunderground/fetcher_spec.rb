require 'spec_helper'

RSpec.describe SmartlinkWeatherData::Wunderground::Fetcher do
  let(:fetcher) { described_class.new "76555" }
  describe "#fetch" do
    let(:result) { fetcher.fetch }
    let(:data) { 'some data' }
    let(:wunderground) { double :wunderground, forecast_for: data }
    before(:each) do
      allow(Wunderground).to receive(:new).and_return wunderground
    end

    it "initializes an api object" do
      expect(Wunderground).to receive(:new).and_return wunderground
      result
    end

    it "fetches the forcast" do
      expect(wunderground).to receive(:forecast_for).with('76555').and_return data
      result
    end

    it "returns the data" do
      expect(result).to eq(data)
    end
  end
end
