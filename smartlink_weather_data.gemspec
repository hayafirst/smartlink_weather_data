# -*- encoding: utf-8 -*-

Gem::Specification.new do |gem|
  gem.authors       = ["Yi Wen"]
  gem.email         = ["hayafirst@gmail.com"]
  gem.description   = %q{Interaction with wunderground}
  gem.summary       = %q{Interaction with wunderground}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "smartlink_weather_data"
  gem.require_paths = ["lib"]
  gem.license       = "MIT"
  gem.version       = "0.0.1"
  gem.add_runtime_dependency(%q<wunderground>)
  gem.add_development_dependency(%q<dotenv>)
  gem.add_development_dependency(%q<timecop>)
  gem.add_development_dependency(%q<standalone_migrations>)
  gem.add_development_dependency(%q<rspec>)
  gem.add_development_dependency(%q<simplecov>)
  gem.add_development_dependency(%q<database_cleaner>)
  gem.add_development_dependency(%q<rake>)
  gem.add_development_dependency(%q<pg>)
end
