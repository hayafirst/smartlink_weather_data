class CreateSmartlinkWeatherDataForecast < ActiveRecord::Migration
  def change
    create_table :smartlink_weather_data_forecasts do |t|
      t.string :postal_code
      t.integer :epoch
      t.date :date
      t.string :time_zone
      t.integer :low_fahrenheit
      t.integer :low_celsius
      t.integer :high_fahrenheit
      t.integer :high_celsius
      t.float :pop, default: 0
      t.float :qpf_allday_inch
      t.float :qpf_allday_mm
      t.float :qpf_day_inch
      t.float :qpf_day_mm
      t.float :qpf_night_inch
      t.float :qpf_night_mm
      t.timestamps null: false
    end
  end
end
