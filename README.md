# SmartlinkWeatherData

This is for connecting to wunderground service and write the data to the DB

## Usage

- Add it to your Gemfile in a Rails app
- run `bundle`
- run `bundle exec rake db:migrate` to create the table used by the enigine
- `SmartlinkWeatherData::Processes::ChanceOfRain.value(postal_codes)` will return a hash like `{'11111' => 40, "22222" => 50}`
- `SmartlinkWeatherData::Processes:SaveData.new(postal_codes).save` will fetch passed postal_codes into the DB for a background job

The Gem can be used independently without a Rails container. It has ability to run migrations and such.

## Environment variables

The environment variables the gem needs:

```ruby
WUNDERGROUND_API_KEY=api key
DATABASE_URL=db_url
RAILS_ENV=your rails env
```

start IRB session with them or use dotenv to set them up.
