require 'dotenv'
require 'active_support/all'
require 'active_record'
Dotenv.load ".env/#{ ENV['RAILS_ENV'] || :development }"
ActiveRecord::Base.establish_connection ENV['DATABASE_URL']
Dir["#{File.dirname(__FILE__)}/smartlink_weather_data/**/*.rb"].each do |f|
  require f
end
