require 'wunderground'
module SmartlinkWeatherData
  module Wunderground
    class Fetcher
      attr_reader :postal_code
      def initialize(postal_code)
        @postal_code = postal_code
      end

      def fetch
        w_api = ::Wunderground.new
        w_api.forecast_for postal_code
      end
    end
  end
end
