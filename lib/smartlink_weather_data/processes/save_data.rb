module SmartlinkWeatherData
  module Processes
    class SaveData
      class << self
        def save(*postal_codes)
          model_class = SmartlinkWeatherData::Models::Forecast
          postal_codes.each do |postal_code|
            result = SmartlinkWeatherData::Wunderground::Fetcher.new(postal_code).fetch
            attrs = data_part(result).map do |forecast|
              date_part = forecast['date']
              date = Date.new(date_part['year'], date_part['month'], date_part['day'])
              if !model_class.date_and_postal_code(date, postal_code).exists?
              {
                postal_code: postal_code,
                epoch: date_part['epoch'],
                date: date,
                time_zone: date_part['tz_short'],
                low_fahrenheit: forecast['low']['fahrenheit'],
                low_celsius: forecast['low']['celsius'],
                high_fahrenheit: forecast['high']['fahrenheit'],
                high_celsius: forecast['high']['celsius'],
                pop: forecast['pop'] || 0,
                qpf_allday_inch: forecast['qpf_allday']['in'],
                qpf_allday_mm: forecast['qpf_allday']['mm'],
                qpf_night_inch: forecast['qpf_night']['in'],
                qpf_night_mm: forecast['qpf_night']['mm'],
                qpf_day_inch: forecast['qpf_day']['in'],
                qpf_day_mm: forecast['qpf_day']['mm']
              }
              end
            end
            model_class.create attrs.compact
          end
        end

        private

        def data_part(response)
          response['forecast']['simpleforecast']['forecastday']
        end
      end
    end
  end
end
