module SmartlinkWeatherData
  module Processes
    class ChanceOfRain
      class << self
        def value(*postal_codes)
          return_value = {}
          day1 = Date.today
          result = SmartlinkWeatherData::Models::Forecast.date_and_postal_code(
            [day1, day1 + 1, day1 + 2], postal_codes
          )
          grouped_result = result.group_by(&:postal_code)
          postal_codes.each do |postal_code|
            forecasts = grouped_result[postal_code]
            if forecasts.try(:size).to_i < 3
              SaveData.save postal_code
              forecasts = SmartlinkWeatherData::Models::Forecast.date_and_postal_code(
                [day1, day1 + 1, day1 + 2], postal_code
              )
            end
            return_value.merge!(postal_code => forecasts.max_by(&:pop).pop) unless forecasts.blank?
          end
          return_value
        end
      end
    end
  end
end
