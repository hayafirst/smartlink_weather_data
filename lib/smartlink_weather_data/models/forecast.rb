module SmartlinkWeatherData
  module Models
    class Forecast < ::ActiveRecord::Base
      self.table_name = "smartlink_weather_data_forecasts"
      validates :date, :pop, :postal_code, presence: true
      validates :date, uniqueness: { scope: :postal_code }

      scope :date_and_postal_code, ->(date, postal_code) { where(date: date, postal_code: postal_code) }
    end
  end
end
