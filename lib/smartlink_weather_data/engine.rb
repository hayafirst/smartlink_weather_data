if(Object.const_defined?("Rails::Engine"))
   module SmartlinkWeatherData
     class Engine < ::Rails::Engine
       isolate_namespace SmartlinkWeatherData
       initializer :append_migrations do |app|
         unless app.root.to_s.match root.to_s
           config.paths["db/migrate"].expanded.each do |expanded_path|
             app.config.paths["db/migrate"] << expanded_path
           end
         end
       end
     end
   end
end
